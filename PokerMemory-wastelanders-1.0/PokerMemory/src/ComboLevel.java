import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ComboLevel extends StraightScore
{
	private TurnScore Score;
	private String[] FlushCards;
	private String[] Pairs;
	private int Points = 0;
	
	public ComboLevel(TurnsTakenCounterLabel validTurnTime, TurnScore Score, JFrame mainFrame) 
	{
		super(validTurnTime, Score, mainFrame);
		this.getTurnsTakenCounter().setDifficultyModeLabel("Combo Level");
		this.setCardsToTurnUp(5);
		this.Score = Score; Score.reset(); 	
		this.getScore().setDifficultyModeLabel("Combo Level");
	}
	
	private TurnScore getScore() 
	{
		return Score;
	}
	
	@Override
	protected boolean turnUp(Card card)
	{
		final JDialog pop = new JDialog();
		pop.setAlwaysOnTop(true);  
		// the card may be turned
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{
			// add the card to the list
			sounds.pikachu();
			this.getTurnedCardsBuffer().add(card);
			
			if(this.getTurnedCardsBuffer().size() == this.getCardsToTurnUp())
			{
				// We are uncovering the last card in this turn
				// Record the player's turn
				this.getTurnsTakenCounter().increment();
				// get the other card (which was already turned up)
				Card otherCard1 = (Card) this.getTurnedCardsBuffer().get(0);
				Card otherCard2 = (Card) this.getTurnedCardsBuffer().get(1);
				Card otherCard3 = (Card) this.getTurnedCardsBuffer().get(2);
				Card otherCard4 = (Card) this.getTurnedCardsBuffer().get(3);
				Card otherCard5 = (Card) this.getTurnedCardsBuffer().get(4);
				
				String[] straightCards = {otherCard5.getRank(), otherCard1.getRank(), otherCard2.getRank(), otherCard3.getRank(), otherCard4.getRank()};	
				int [] straightCardsIndex = new int[5];
				
					for(int i = 0; i < 13; i++)
					{	
						for(int j = 0; j < 5; j++)
						{
							if(ranks[i].equals(straightCards[j]))
							{
								straightCardsIndex[j] = i;
							}
						}
					}
				Arrays.sort(straightCardsIndex);	
				
				//PairIndex array creation and sorting.
				Pairs = new String[] {card.getRank(), otherCard1.getRank(), otherCard2.getRank(), otherCard3.getRank(), otherCard4.getRank()};
				int [] PairIndex = new int[5];
				
					for(int i = 0; i < 13; i++) 
					{	
						for(int j = 0; j < 5; j++) 
						{
							if(ranks[i].equals(Pairs[j])) 
							{
								PairIndex[j] = i;
							}
						}
					}
				Arrays.sort(PairIndex);
				
				//TrioPairIndex array creation and sorting.
				int [] TrioPairIndex = new int[5];
				
					for(int i = 0; i < 13; i++) 
					{	
						for(int j = 0; j < 5; j++) 
						{
							if(ranks[i].equals(Pairs[j])) 
							{
								TrioPairIndex[j] = i;
							}
						}
					}
				Arrays.sort(TrioPairIndex);
				
				//FullHandIndex array creation and sorting.
				int [] FullHandIndex = new int[5];
				
					for(int i = 0; i < 13; i++) 
					{	
						for(int j = 0; j < 5; j++)
						{
							if(ranks[i].equals(Pairs[j])) 
							{
								FullHandIndex[j] = i;
							}
						}
					}
				Arrays.sort(FullHandIndex);
	
				//Straight Level
				if((straightCardsIndex[0] + 1 == straightCardsIndex[1]) && (straightCardsIndex[1] + 1 == straightCardsIndex[2]) && (straightCardsIndex[2] + 1 == straightCardsIndex[3]) && straightCardsIndex[4] == 12)
				{
					card.faceUp();
					this.getTurnedCardsBuffer().clear();
					Points= 0;
					Points= Points + 1000;
					int max = straightCardsIndex[4];
					
						for (int i = 0; i < straightCardsIndex.length; i++)
						{
								if (straightCardsIndex[i] > max) 
								{
									max = straightCardsIndex[i];
								}
						}	
						if(max == (12))
						{
							Points=Points+(100*20);
						}
						else 
						{
							Points=Points+((max + 2)*100);
						} 
					// Pop up letting you know there are 3 hands and you can choose which one you want.
					JOptionPane.showMessageDialog(pop,"A Straight!! Total Points: " + Points);
					int yes = JOptionPane.showConfirmDialog(pop,"Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
					
					if(yes==0) 
					{
						sounds.accomplished();
						this.getScore().increment(-5);
						this.getTurnDownTimer().start();
					}
					else 
					{
						sounds.worked();
						this.getScore().increment(Points);
						this.getTurnedCardsBuffer().clear();
					}
						
				}
				
				else if((straightCardsIndex[0] + 1 == straightCardsIndex[1]) && (straightCardsIndex[1] + 1 == straightCardsIndex[2]) && (straightCardsIndex[2] + 1 == straightCardsIndex[3]) && (straightCardsIndex[3] + 1 == straightCardsIndex[4]))
				{
					// 5 cards match, so remove them from the list (they will remain face up)
					card.faceUp();
					this.getTurnedCardsBuffer().clear();
					Points= 0;
					Points= Points + 1000;
					int max = straightCardsIndex[4];
					
						for (int i = 0; i < straightCardsIndex.length; i++) 
						{
								if (straightCardsIndex[i] > max)
								{
									max = straightCardsIndex[i];
								}
						}	
						if(max == (12)) 
						{
							Points=Points+(100*20);
						}
						else 
						{
							Points=Points+((max + 2)*100);
						} 
						
					// Pop up letting you know there are 3 hands and you can choose which one you want.
					JOptionPane.showMessageDialog(pop,"A Straight!! Total Points: " + Points);
					int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
					
					if(yes==0) 
					{
						sounds.accomplished();
						this.getScore().increment(-5);
						this.getTurnDownTimer().start();
					}
					else 
					{
						sounds.worked();
						this.getScore().increment(Points);
						this.getTurnedCardsBuffer().clear();
						
					}
				}
				
				//flush level
				else if((card.getSuit().equals(otherCard1.getSuit())) && (card.getSuit().equals(otherCard2.getSuit())) && (card.getSuit().equals(otherCard3.getSuit())) && (card.getSuit().equals(otherCard4.getSuit())) ) 
				{
					card.faceUp();
					//creates a new array for the ranks in the hand.
					FlushCards = new String[] {card.getRank(), otherCard1.getRank(), otherCard2.getRank(), otherCard3.getRank(), otherCard4.getRank()};
					
					Points= Points + 700;
					
						for(int i = 0; i < 5; i++)
						{
							if((FlushCards[i].equals("a")))
							{
								Points= Points + 20;
							}
							else if((FlushCards[i].equals("k")))
							{
								Points= Points +13;
							}
							else if((FlushCards[i].equals("q"))) 
							{
								Points= Points + 12;
							}
							else if((FlushCards[i].equals("j")))
							{
								Points= Points + 11;
							}
							else if((FlushCards[i].equals("t")))
							{
								Points= Points + 10;
							}
							else 
							{
								Points= Points + (Integer.valueOf(FlushCards[i]));
							}
						}
					// Pop up letting you know there are 3 hands and you can choose which one you want.
					JOptionPane.showMessageDialog(pop,"A Flush!! Total Points: " + Points);
					int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
					
					if(yes==0) 
					{
						sounds.accomplished();
						this.getScore().increment(-5);
						this.getTurnDownTimer().start();
					}
					else
					{
						sounds.worked();
						this.getScore().increment(Points);
						this.getTurnedCardsBuffer().clear();
					}	
				}
				
				// Pair Hand
				else if (PairIndex[0] == PairIndex[1] && PairIndex[1] != PairIndex[2] && PairIndex[2] != PairIndex[3] && PairIndex[3] != PairIndex[4] || 
					PairIndex[1] == PairIndex[2] && PairIndex[2] != PairIndex[3] && PairIndex[3] != PairIndex[4] && PairIndex[0] != PairIndex[1] || 
				    PairIndex[2] == PairIndex[3] && PairIndex[3] != PairIndex[4] && PairIndex[0] != PairIndex[1] && PairIndex[1] != PairIndex[2] ||
				    PairIndex[3] != PairIndex[2] && PairIndex[3] == PairIndex[4] && PairIndex[0] != PairIndex[1] && PairIndex[1] != PairIndex[2] ) 
				{
					card.faceUp();
					Points= 0;
					Points = Points + 200;
					
						for(int i = 0; i < 5; i++) 
						{
					      if( PairIndex[4] == 12)
					      {
					    	  Points = Points + 20;
					    	  break;
					      }
					      else if(PairIndex[4] == 11)
					      {
					    	  Points = Points + 13;
					    	  break;
					      }
					      else if(PairIndex[4] == 10) 
					      {
					    	  Points = Points + 12;
					    	  break;
					      }
					      else if(PairIndex[4] == 9)
					      {
					    	  Points = Points + 11;
					    	  break;
					      }
						}
					// Pop up letting you know there are 3 hands and you can choose which one you want.
					JOptionPane.showMessageDialog(pop,"A Pair!! Total Points: " + Points);
					int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
					
					if(yes==0) 
					{
						sounds.accomplished();
						this.getScore().increment(-5);
						this.getTurnDownTimer().start();
					}
					else 
					{
						sounds.worked();
						this.getScore().increment(Points);
						this.getTurnedCardsBuffer().clear();
					}
				}
				
				//TrioPair with choice for Pair.
				else if (TrioPairIndex[0] == TrioPairIndex[1] && TrioPairIndex[1] == TrioPairIndex[2] && TrioPairIndex[3] != TrioPairIndex[4] && TrioPairIndex[3] != TrioPairIndex[2] || 
					   	 TrioPairIndex[1] == TrioPairIndex[2] && TrioPairIndex[2] == TrioPairIndex[3] && TrioPairIndex[0] != TrioPairIndex[1] && TrioPairIndex[3] != TrioPairIndex[4] || 
						 TrioPairIndex[2] == TrioPairIndex[3] && TrioPairIndex[3] == TrioPairIndex[4] && TrioPairIndex[0] != TrioPairIndex[1] && TrioPairIndex[1] != TrioPairIndex[2] )
				{
					// Pop up letting you know there are 2 hands and you can choose which one you want.
					card.faceUp();
					
					String[] options = new String[] {"Pair Hand", "Trio Hand", "Cancel"};
				    int response = JOptionPane.showOptionDialog(pop, "Make a Choice!", "You Got 2 Hands!!",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null, options, options[0]);
				    
				    // respond 0 equals Pair hand so it evaluates the hand as a pair.
				    if(response == 0) 
				    {
						Points= 0;
						Points = Points + 200;
						
							for(int i = 0; i < 5; i++) 
							{
								if( PairIndex[4] == 12)
								{
									Points = Points + 20;
						    	  	break;
								}
								else if(PairIndex[4] == 11)
								{
									Points = Points + 13;
									break;
								}
								else if(PairIndex[4] == 10) 
								{
									Points = Points + 12;
									break;
								}
								else if(PairIndex[4] == 9)
								{
									Points = Points + 11;
									break;
								}
							}
						JOptionPane.showMessageDialog(pop,"A Pair!! Total Points: " + Points);
						int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
						
						if(yes==0) 
						{
							sounds.accomplished();
							this.getScore().increment(-5);
							this.getTurnDownTimer().start();
							
						}
						else 
						{
							sounds.worked();
							this.getScore().increment(Points);
							this.getTurnedCardsBuffer().clear();
						}
					}
				   
				    else if(response == 1) 
				    {
				    	Points= 0;
				    	Points = Points + 300;
				    		for(int i = 0; i < 5; i++)
				    		{
					      
				    			if( TrioPairIndex[4] == 12)
				    			{
				    				Points = Points + 20;
				    				break;
				    			}
				    			else if(TrioPairIndex[4] == 11)
				    			{
				    				Points = Points + 13;
								break;
				    			}
				    			else if(TrioPairIndex[4] == 10) 
				    			{
				    				Points = Points + 12;
				    				break;
				    			}
				    			else if(TrioPairIndex[4] == 9)
				    			{
				    				Points = Points + 11;
				    				break;
				    			}
				    		}
					
					JOptionPane.showMessageDialog(pop,"A TrioPair!! Total Points: " + Points);
					int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
					
					if(yes==0) 
					{
						sounds.accomplished();
						this.getScore().increment(-5);
						this.getTurnDownTimer().start();
						
					}
					else
					{
						sounds.worked();
						this.getScore().increment(Points);
						this.getTurnedCardsBuffer().clear();
					}
				    }
				}
				
				//Full House with choice to trio and pair hands				
				else if (FullHandIndex[0] == FullHandIndex[1] && FullHandIndex[1] == FullHandIndex[2] && FullHandIndex[3] == FullHandIndex[4] ||  
						 FullHandIndex[2] == FullHandIndex[3] && FullHandIndex[3] == FullHandIndex[4] && FullHandIndex[0] == FullHandIndex[1] ) 
				{
					card.faceUp();
					// Pop up letting you know there are 3 hands and you can choose which one you want.
					String[] options = new String[] {"Pair Hand", "Trio Hand", "Full House","Cancel"};
				    int response = JOptionPane.showOptionDialog(pop, "Make a Choice!", "You Got 3 Hands!!",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
				    // respond 0 equals Pair hand so it evaluates the hand as a pair.
					if(response == 0) 
					{
						Points= 0;
						Points = Points + 200;
							for(int i = 0; i < 5; i++) 
							{  
								if( PairIndex[4] == 12)
								{
									Points = Points + 20;
									break;
								}
								else if(PairIndex[4] == 11)
								{
									Points = Points + 13;
									break;
								}
								else if(PairIndex[4] == 10) 
								{
									Points = Points + 12;
									break;
								}
								else if(PairIndex[4] == 9)
								{
									Points = Points + 11;
									break;
								}
							}
						// pop up letting the player know the score and if he wants to pass.
						JOptionPane.showMessageDialog(pop,"A Pair!! Total Points: " + Points);
						int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
						
						if(yes==0) 
						{
							sounds.accomplished();
							this.getScore().increment(-5);
							this.getTurnDownTimer().start();
							
						}
						else 
						{
							sounds.worked();
							this.getScore().increment(Points);
							this.getTurnedCardsBuffer().clear();
						}
					}
					
					// respond 1 equals Trio hand so it evaluates the hand as a TrioPair.
					else if(response == 1) 
					{
						Points= 0;
						Points = Points + 300;
						//searches the array for the highest int.
							for(int i = 0; i < 5; i++) 
							{  
								if( TrioPairIndex[4] == 12)
								{
									Points = Points + 20;
									break;
								}
								else if(TrioPairIndex[4] == 11)
								{
									Points = Points + 13;
									break;
								}
								else if(TrioPairIndex[4] == 10) 
								{
									Points = Points + 12;
									break;
								}
								else if(TrioPairIndex[4] == 9)
								{
									Points = Points + 11;
									break;
								}
							}
							
						// pop ip letting the player know the score and if he wants to pass.
						JOptionPane.showMessageDialog(pop,"A TrioPair!! Total Points: " + Points);
						int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
						
						if(yes==0) 
						{
							sounds.accomplished();
							this.getScore().increment(-5);
							this.getTurnDownTimer().start();
							
						}
						else 
						{	
							sounds.worked();
							this.getScore().increment(Points);
							this.getTurnedCardsBuffer().clear();
						
						}
					}
					
					// respond 2 equals Full house so it evaluates the hand as a Full House.
					else if(response == 2)
					{
						Points = Points + 1500;
						
							for(int i = 0; i < 5; i++)
							{
								if( FullHandIndex[4] == 12)
								{
									Points = Points +20;
									break;
								}
								else if(FullHandIndex[4] == 11)
								{
									Points = Points +13;
									break;
								}
								else if(FullHandIndex[4] == 10) 
								{
									Points = Points + 12;
									break;
								}
								else if(FullHandIndex[4] == 9)
								{
									Points = Points + 11;
									break;
								}
							}
							
						JOptionPane.showMessageDialog(pop,"Full Hand!! Total Points: " + Points);
						int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
						
						if(yes==0) 
						{
							sounds.accomplished();
							this.getScore().increment(-5);
							this.getTurnDownTimer().start();
							
						}
						else 
						{	
							sounds.worked();
							this.getScore().increment(Points);
							this.getTurnedCardsBuffer().clear();
						}
					}
					else
					{
						this.getTurnDownTimer().start();
					}
				}
				//Two Pair
				else if (PairIndex[0] == PairIndex[1] && PairIndex[2] == PairIndex[3] && PairIndex[3] != PairIndex[4]  || 
						 PairIndex[1] == PairIndex[2] && PairIndex[3] == PairIndex[4] && PairIndex[0] != PairIndex[1]  || 
					     PairIndex[0] == PairIndex[1] && PairIndex[3] == PairIndex[4] && PairIndex[3] != PairIndex[2] && PairIndex[1] != PairIndex[2] ) 
					{
						card.faceUp();
						Points= 0;
						Points = Points + 400;
						
							for(int i = 0; i < 5; i++) 
							{
						      if( PairIndex[4] == 12)
						      {
						    	  Points = Points + 20;
						    	  break;
						      }
						      else if(PairIndex[4] == 11)
						      {
						    	  Points = Points + 13;
						    	  break;
						      }
						      else if(PairIndex[4] == 10) 
						      {
						    	  Points = Points + 12;
						    	  break;
						      }
						      else if(PairIndex[4] == 9)
						      {
						    	  Points = Points + 11;
						    	  break;
						      }
							}
						// Pop up letting you know there are 3 hands and you can choose which one you want.   
						JOptionPane.showMessageDialog(pop,"A Two Pair!! Total Points: " + Points);
						int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
						
						if(yes==0) 
						{
							sounds.accomplished();
							this.getScore().increment(-5);
							this.getTurnDownTimer().start();
						}
						else 
						{
							sounds.worked();
							this.getScore().increment(Points);
							this.getTurnedCardsBuffer().clear();
						}
					}
				
				//High Card
				else
				{
					card.faceUp();
					Points= 0;
						
					      if( PairIndex[4] == 12)
					      {
					    	  Points = Points + 20;
					      }
					      else
					      {
					    	  Points = Points + Integer.valueOf(PairIndex[4] + 2);
					      }
						
						JOptionPane.showMessageDialog(pop,"High Card Total Points: " + Points);
						int yes = JOptionPane.showConfirmDialog(pop, "Want To Pass?", "Happy With The Score?", JOptionPane.YES_NO_OPTION);
						
						if(yes==0) 
						{
							sounds.accomplished();
							this.getScore().increment(-5);
							this.getTurnDownTimer().start();
							
						}
						else 
						{	
							sounds.worked();
							this.getScore().increment(Points);
							this.getTurnedCardsBuffer().clear();
						}
				}		
			}
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean  isGameOver(){
		for (int i =0; i< this.getGrid().size();i++)
			if(!this.getGrid().get(i).isFaceUp())
				return false;


		return true;
	}
}



import javax.swing.JFrame;

public class EqualPairScore extends EqualPairLevel {


	private TurnScore Score;

	protected EqualPairScore(TurnsTakenCounterLabel validTurnTime, TurnScore Score, JFrame mainFrame) {
		super(validTurnTime, mainFrame);
		super.getTurnsTakenCounter().setDifficultyModeLabel("Medium Level");
		this.Score = Score; Score.reset(); 
		this.getScore().setDifficultyModeLabel("Medium Level");
	}

	GameSounds sounds = new GameSounds();
	
	@Override
	protected boolean turnUp(Card card) {
		
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{	
			sounds.pikachu();
			this.getTurnedCardsBuffer().add(card);
			if(this.getTurnedCardsBuffer().size() == getCardsToTurnUp())
			{
				this.getTurnsTakenCounter().increment();
				Card otherCard = (Card) this.getTurnedCardsBuffer().get(0);
				// the the 2 cards match so the score will increase by 50 points.
				if( otherCard.getNum() == card.getNum()) {
					sounds.worked();
					this.getTurnedCardsBuffer().clear();
					this.getScore().increment(50);
				}
				// the cards don't match and the score decreases.
				else {
					sounds.accomplished();
					this.getTurnDownTimer().start();
					this.getScore().increment(-5);
				}
			}
			return true;
		}
		return false;
	}

	private TurnScore getScore() {
		// TODO Auto-generated method stub
		return Score;
	}


}

import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class FlushLevel extends RankTrioLevel{

	protected FlushLevel(TurnsTakenCounterLabel validTurnTime, JFrame mainFrame) {
		super(validTurnTime, mainFrame);
		this.getTurnsTakenCounter().setDifficultyModeLabel("Flush Level");
		this.setCardsToTurnUp(5);
	}
	

	@Override
	protected void makeDeck() {
		// In Flush level the grid consists of distinct cards, no repetitions

		//back card
		ImageIcon backIcon = this.getCardIcons()[this.getTotalCardsPerDeck()];

		int cardsToAdd[] = new int[getRowsPerGrid() * getCardsPerRow()];
		// Four variables to update four different for loops so that each loop searches for a specific 
		int i_1 = 0;
		int j_1 = 1;
		int k_1 = 2;
		int n_1 = 3;
		
		for(int i = 0; i < 15; i++)
		{	
			if(i_1 >= 52)
			{
				i_1 = 0;
				cardsToAdd[i] = i_1;
				i_1 += 4;
			}
			else
			{
				cardsToAdd[i] = i_1;
				i_1 += 4;
			}
		}
		
		
		for(int j = 15; j < 30; j++)
		{		
			if(j_1 >= 52)
			{
				j_1 = 1;
				cardsToAdd[j] = j_1;
				j_1 += 4;
			}	
			else
			{
				cardsToAdd[j] = j_1;
				j_1 += 4;
			}
		}
		
		
		for(int k = 30; k < 40; k++)
		{
			if(k_1 >= 52)
			{
				k_1 = 2;
				cardsToAdd[k] = k_1;
				k_1 += 4;
			}			
			else
			{
				cardsToAdd[k] = k_1;
				k_1 += 4;
			}
		}
		
		
		for(int n = 40; n < 50; n++)
		{
			if(n_1 >= 52)
			{
				n_1 = 3;
				cardsToAdd[n] = n_1;
				n_1 += 4;
			}
			else 
			{
				cardsToAdd[n] = n_1;
				n_1 += 4;
			}
		}

		// randomize the order of the deck
		this.randomizeIntArray(cardsToAdd);

		// make each card object
		for(int i = 0; i < cardsToAdd.length; i++)
		{
			// number of the card, randomized
			int num = cardsToAdd[i];
			// make the card object and add it to the panel
			String rank = cardNames[num].substring(0, 1);
			String suit = cardNames[num].substring(1, 2);
			this.getGrid().add( new Card(this, this.getCardIcons()[num], backIcon, num, rank, suit));
		}
}
	
	@Override
	protected boolean turnUp(Card card) {
		// the card may be turned
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{
			// add the card to the list
			this.getTurnedCardsBuffer().add(card);
			if(this.getTurnedCardsBuffer().size() == this.getCardsToTurnUp())
			{
				// We are uncovering the last card in this turn
				// Record the player's turn
				this.getTurnsTakenCounter().increment();
				// get the other card (which was already turned up)
				Card otherCard1 = (Card) this.getTurnedCardsBuffer().get(0);
				Card otherCard2 = (Card) this.getTurnedCardsBuffer().get(1);
				Card otherCard3 = (Card) this.getTurnedCardsBuffer().get(2);
				Card otherCard4 = (Card) this.getTurnedCardsBuffer().get(3);
				if((card.getSuit().equals(otherCard1.getSuit())) && (card.getSuit().equals(otherCard2.getSuit())) && (card.getSuit().equals(otherCard3.getSuit())) && (card.getSuit().equals(otherCard4.getSuit())) ) {
					// 5 cards match, so remove them from the list (they will remain face up)
					this.getTurnedCardsBuffer().clear();
				}
				else
				{
					// The cards do not match, so start the timer to turn them down
					this.getTurnDownTimer().start();
				}
			}
			return true;
		}
		return false;
	}
	
}

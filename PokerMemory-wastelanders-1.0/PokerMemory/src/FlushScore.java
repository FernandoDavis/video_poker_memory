
import javax.swing.JFrame;

public class FlushScore extends FlushLevel {

	private TurnScore Score;
	private String[] FlushCards;
	protected FlushScore(TurnsTakenCounterLabel validTurnTime,TurnScore Score, JFrame mainFrame) {
		super(validTurnTime, mainFrame);
		this.getTurnsTakenCounter().setDifficultyModeLabel("Flush Level");
		this.setCardsToTurnUp(5);
		this.Score = Score; Score.reset(); 
		this.getScore().setDifficultyModeLabel("Flush Level");
	}
	
	private TurnScore getScore() {
		// TODO Auto-generated method stub
		return Score;
	}
	
	@Override
	protected boolean turnUp(Card card) {
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{
			sounds.pikachu();
			this.getTurnedCardsBuffer().add(card);
			if(this.getTurnedCardsBuffer().size() == this.getCardsToTurnUp())
			{
				
				this.getTurnsTakenCounter().increment();
			
				Card otherCard1 = (Card) this.getTurnedCardsBuffer().get(0);
				Card otherCard2 = (Card) this.getTurnedCardsBuffer().get(1);
				Card otherCard3 = (Card) this.getTurnedCardsBuffer().get(2);
				Card otherCard4 = (Card) this.getTurnedCardsBuffer().get(3);
				if((card.getSuit().equals(otherCard1.getSuit())) && (card.getSuit().equals(otherCard2.getSuit())) && (card.getSuit().equals(otherCard3.getSuit())) && (card.getSuit().equals(otherCard4.getSuit())) ) {
					// 5 cards match, so remove them from the list (they will remain face up)
					sounds.worked();
					this.getTurnedCardsBuffer().clear();
					this.getScore().increment(700);
					
					FlushCards = new String[] {card.getRank(), otherCard1.getRank(), otherCard2.getRank(), otherCard3.getRank(), otherCard4.getRank()};
					// the score increments by 700 plus an increment depending on the rank.
					for(int i = 0; i < 5; i++) {
						if((FlushCards[i].equals("a"))) {
							this.getScore().increment(20);
							}
						else if((FlushCards[i].equals("k"))) {
							this.getScore().increment(13);
							}
						else if((FlushCards[i].equals("q"))) {
							this.getScore().increment(12);
							}
						else if((FlushCards[i].equals("j"))) {
							this.getScore().increment(11);
							}
						else if((FlushCards[i].equals("t"))) {
							this.getScore().increment(10);
							}
						else 
							{
							this.getScore().increment(Integer.valueOf(FlushCards[i]));
							}
				}
				}
				else
				{
					// The cards do not match, so the score decreases by 5 points.
					sounds.accomplished();
					this.getTurnDownTimer().start();
					this.getScore().increment(-5);
				}
			}
			return true;
		}
		return false;
	}
}
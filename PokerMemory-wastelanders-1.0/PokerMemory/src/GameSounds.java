import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class GameSounds {
	
	Clip clip; //initiates clip object to store the sound and play/stop/loop/reset
	
	public void pikachu() {
		String pikachu = "sounds/pikachu.wav";
	    File file = new File(pikachu); //Creates file of general path
	    String path = file.getAbsolutePath(); //Gets absolute path on each different pc of file and stores it in a string
	    File filePath = new File(path); //Uses path string to search file on the absolute path   
	    try {
	        clip = AudioSystem.getClip();
	        AudioInputStream pikachuSound = AudioSystem.getAudioInputStream(filePath); //Stores sound on path specified
	        clip.open(pikachuSound); //Opens sound on path specified
	        clip.start(); //Starts sound
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void pikachuSad() {
		String pikachusad = "sounds/pikasad.wav";
	    File file = new File(pikachusad);
	    String path = file.getAbsolutePath();
	    File filePath = new File(path);    
	    try {
	        clip = AudioSystem.getClip();
	        AudioInputStream pikachuSadSound = AudioSystem.getAudioInputStream(filePath);
	        clip.open(pikachuSadSound);
	        clip.start();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void accomplished() {
		String accomplished = "sounds/accomplished.wav";
	    File file = new File(accomplished);
	    String path = file.getAbsolutePath();
	    File filePath = new File(path);    
	    try {
	        clip = AudioSystem.getClip();
	        AudioInputStream accomplishedSound = AudioSystem.getAudioInputStream(filePath);
	        clip.open(accomplishedSound);
	        clip.start();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void worked() {
		String worked = "sounds/worked.wav";
	    File file = new File(worked);
	    String path = file.getAbsolutePath();
	    File filePath = new File(path);    
	    try {
	        clip = AudioSystem.getClip();
	        AudioInputStream workedSound = AudioSystem.getAudioInputStream(filePath);
	        clip.open(workedSound);
	        clip.start();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void confused() {
		String confused = "sounds/confused.wav";
	    File file = new File(confused);
	    String path = file.getAbsolutePath();
	    File filePath = new File(path);    
	    try {
	        clip = AudioSystem.getClip();
	        AudioInputStream confusedSound = AudioSystem.getAudioInputStream(filePath);
	        clip.open(confusedSound);
	        clip.start();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

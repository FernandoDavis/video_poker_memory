import java.io.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Music{
	static Clip clip;
	
	public static void music(){
    String backgroundMusic = "music/LavenderTown.wav";
    File file = new File(backgroundMusic);
    String path = file.getAbsolutePath();
    File filePath = new File(path);
     
    try {
        clip = AudioSystem.getClip();
        AudioInputStream lavenderTown = AudioSystem.getAudioInputStream(filePath);
        clip.open(lavenderTown);
        clip.setFramePosition(0);
        clip.start();
        FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN); //Lowers the volume of the music so that other sounds are heard over it
        	volume.setValue(-18.0f); //Sets decibels of clip
        	
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (LineUnavailableException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnsupportedAudioFileException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}    
	}
}
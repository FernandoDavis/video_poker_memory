 import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class NewMemoryFrame extends MemoryFrame {
	public void MemoryFrame() {

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -609538564388064538L;
	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final boolean DEBUG = true;
	private JPanel contentPane;
	private TurnsTakenCounterLabel turnCounterLabel;
	private GameLevel difficulty;
	private JPanel centerGrid;
	private JLabel levelDescriptionLabel;
	private TurnScore Score;
	/**
	 * Create the frame.
	 * @return 
	 */
	public NewMemoryFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800,700);

		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("Levels");
		menuBar.add(mnFile);
		
		ActionListener menuHandler = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dprintln("actionPerformed " + e.getActionCommand());
				try {
					if(e.getActionCommand().equals("Easy Level")) newGame("easy");
					else if(e.getActionCommand().equals("Equal Pair Level")) newGame("equalpair");
					else if(e.getActionCommand().equals("Same Rank Trio Level")) newGame("ranktrio");
					else if(e.getActionCommand().equals("Flush Level")) newGame("flush");
					else if(e.getActionCommand().equals("Straight Level")) newGame("straight");
					else if(e.getActionCommand().equals("Combo Level")) newGame("combo");
					else if(e.getActionCommand().equals("How To Play")) showInstructions();
					else if(e.getActionCommand().equals("About")) showAbout();
					else if(e.getActionCommand().equals("Mute")) Music.clip.stop();
					else if(e.getActionCommand().equals("Play")) {
						Music.clip.setFramePosition(0);
						Music.clip.start();
					}
					else if(e.getActionCommand().equals("Cheats")) showCheats();
					else if(e.getActionCommand().equals("Exit")) System.exit(0);
				} catch (IOException e2) {
					e2.printStackTrace(); throw new RuntimeException("IO ERROR");
				}
			}
		};

		JMenuItem easyLevelMenuItem = new JMenuItem("Easy Level");
		easyLevelMenuItem.addActionListener(menuHandler);
		mnFile.add(easyLevelMenuItem);

		JMenuItem equalPairMenuItem = new JMenuItem("Equal Pair Level");
		equalPairMenuItem.addActionListener(menuHandler);
		mnFile.add(equalPairMenuItem);
		
		JMenuItem sameRankTrioMenuItem = new JMenuItem("Same Rank Trio Level");
		sameRankTrioMenuItem.addActionListener(menuHandler);		
		mnFile.add(sameRankTrioMenuItem);
		
		JMenuItem FlushMenuItem = new JMenuItem("Flush Level");
		FlushMenuItem.addActionListener(menuHandler);		
		mnFile.add(FlushMenuItem);
		
		JMenuItem straightMenuItem = new JMenuItem("Straight Level");
		straightMenuItem.addActionListener(menuHandler);		
		mnFile.add(straightMenuItem);
		
		JMenuItem comboMenuItem = new JMenuItem("Combo Level");
		comboMenuItem.addActionListener(menuHandler);		
		mnFile.add(comboMenuItem);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmHowToPlay = new JMenuItem("How To Play");
		mntmHowToPlay.addActionListener(menuHandler);
		mnHelp.add(mntmHowToPlay);
		
		JMenuItem mntmCheats = new JMenuItem("Cheats");
		mntmCheats.addActionListener(menuHandler);
		mnHelp.add(mntmCheats);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(menuHandler);
		mnHelp.add(mntmAbout);
		
		//Creates a menu item that mutes/plays the background music
		JMenu mnMusic = new JMenu("Music");
		menuBar.add(mnMusic);
		
		//Mutes music by calling the stop function
		JMenuItem mntmMute = new JMenuItem("Mute");
		mntmMute.addActionListener(menuHandler);
		mnMusic.add(mntmMute);
		
		//Plays music by calling the play function
		JMenuItem mntmPlay = new JMenuItem("Play");
		mntmPlay.addActionListener(menuHandler);
		mnMusic.add(mntmPlay);
	
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(5, 5));

		JLabel lblPokerMemory = new JLabel("Poker Memory: Pokemon Edition");
		lblPokerMemory.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblPokerMemory, BorderLayout.NORTH);
	
		centerGrid = new JPanel();
		centerGrid.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		contentPane.add(centerGrid, BorderLayout.CENTER);
		centerGrid.setLayout(new GridLayout(4, 4, 5, 5));

		Component horizontalStrut = Box.createHorizontalStrut(10);
		contentPane.add(horizontalStrut, BorderLayout.WEST);

		Component horizontalStrut_1 = Box.createHorizontalStrut(10);
		contentPane.add(horizontalStrut_1, BorderLayout.EAST);		
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_2);

		JLabel lblNewLabel = new JLabel("Clicks:");
		panel_1.add(lblNewLabel);
		
		turnCounterLabel = new TurnsTakenCounterLabel();
		turnCounterLabel.setText("");
		panel_1.add(turnCounterLabel);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel_1.add(horizontalGlue);
		
		levelDescriptionLabel = new JLabel("New label");
		panel_1.add(levelDescriptionLabel);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_1.add(horizontalGlue_1);

		JLabel pointsLabel = new JLabel("Score:");
		panel_1.add(pointsLabel);

		Score = new TurnScore();
		Score.setText("");
		Score.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_1.add(Score);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_3);
	}

	GameSounds sounds = new GameSounds();
	
	public TurnScore getScore() {
		return Score;
	}
	public TurnsTakenCounterLabel getTurnCounterLabel() {
		return turnCounterLabel;
	}

	public JPanel getCenterGrid() {
		return centerGrid;
	}

	public JLabel getLevelDescriptionLabel() {
		return levelDescriptionLabel;
	}

	public void setTurnCounterLabel(TurnsTakenCounterLabel turnCounterLabel) {
		this.turnCounterLabel = turnCounterLabel;
	}

	public void setCenterGrid(JPanel centerGrid) {
		this.centerGrid = centerGrid;
	}
	
	public void setLevelDescriptionLabel(JLabel levelDescriptionLabel) {
		this.levelDescriptionLabel = levelDescriptionLabel;
	}
	
	public void setGameLevel(GameLevel l) {
		this.difficulty = l;
	}
	
	public void setScore(TurnScore Score) {
		this.Score = Score;
	}

	/**
	 * Prepares a new game (first game or non-first game)
	 * @throws IOException 
	 */
	@Override
	public void newGame(String difficultyMode) throws IOException
	{
		// Reset the turn counter label
		this.getTurnCounterLabel().reset();
		this.getScore().reset();

		// makes a new card field with the score classes.

		if(difficultyMode.equalsIgnoreCase("easy")) {
			this.setGameLevel(new EasyLevel(this.getTurnCounterLabel(), this));
			this.getLevelDescriptionLabel().setText("Easy Level");
		}
		else if(difficultyMode.equalsIgnoreCase("equalpair")){
			this.setGameLevel(new EqualPairScore(this.getTurnCounterLabel(),this.getScore(), this));
			this.getLevelDescriptionLabel().setText("Equal Pair Level");
		}

		else if(difficultyMode.equalsIgnoreCase("ranktrio")){
			this.setGameLevel(new RankTrioScore(this.getTurnCounterLabel(),this.getScore(), this));
			this.getLevelDescriptionLabel().setText("Same Rank Trio Level");
		}
		else if(difficultyMode.equalsIgnoreCase("flush")){
			this.setGameLevel(new FlushScore(this.getTurnCounterLabel(),this.getScore(), this));
			this.getLevelDescriptionLabel().setText("Flush Level");
		}
		else if(difficultyMode.equalsIgnoreCase("straight")){
			this.setGameLevel(new StraightScore(this.getTurnCounterLabel(),this.getScore(), this));
			this.getLevelDescriptionLabel().setText("Straight Level");
		}
		else if(difficultyMode.equalsIgnoreCase("combo")){
			this.setGameLevel(new ComboLevel(this.getTurnCounterLabel(),this.getScore(), this));
			this.getLevelDescriptionLabel().setText("Combo Level");
		}

		else {
			throw new RuntimeException("Illegal Game Level Detected");
		}

		this.turnCounterLabel.reset();
		this.Score.reset();
		// clear out the content pane (removes turn counter label and card field)
		BorderLayout bl  = (BorderLayout) this.getContentPane().getLayout();
		this.getContentPane().remove(bl.getLayoutComponent(BorderLayout.CENTER));
		this.getContentPane().add(showCardDeck(), BorderLayout.CENTER);

		// show the window (in case this is the first game)
		this.setVisible(true);
	}
	
	public JPanel showCardDeck()
	{
		// make the panel to hold all of the cards
		JPanel panel = new JPanel(new GridLayout(difficulty.getRowsPerGrid(),difficulty.getCardsPerRow()));
		
		// this set of cards must have their own manager
		this.difficulty.makeDeck();

		for(int i= 0; i<difficulty.getGrid().size();i++){
			panel.add(difficulty.getGrid().get(i));
		}
		return panel;
	}
	
	public boolean gameOver() throws FileNotFoundException, InterruptedException{
		return difficulty.isGameOver();
	}

	/**
	 * Shows an instructional dialog box to the user
	 */
	private void showInstructions()
	{
		sounds.confused();
		JTextArea text = new JTextArea(25,40);
		dprintln("MemoryGame.showInstructions()");
		final String HOWTOPLAYTEXT = 
				"How To Play\r\n" +
						"\r\n" +
						"EQUAL PAIR Level\r\n"+
						"The game consists of 8 pairs of cards.  At the start of the game,\r\n"+
						"every card is face down.  The object is to find all the pairs and\r\n"+
						"turn them face up.\r\n"+
						"\r\n"+
						"Click on two cards to turn them face up. If the cards are the \r\n"+
						"same, then you have discovered a pair.  The pair will remain\r\n"+
						"turned up.  If the cards are different, they will flip back\r\n"+
						"over automatically after a short delay.  Continue flipping\r\n"+
						"cards until you have discovered all of the pairs.  The game\r\n"+
						"is won when all cards are face up.\r\n"+
						"Each time you flip two cards up, the turn counter will\r\n"+
						"increase.  Try to win the game in the fewest number of turns!"+
						"\r\n"+
						"\r\n"+
						"SAME RANK TRIO Level\r\n"+
						"The game consists of a grid of distinct cards.  At the start of the game,\r\n"+
						"every card is face down.  The object is to find all the trios \r\n"+
						"of cards with the same rank and turn them face up.\r\n"+
						"\r\n"+
						"Click on three cards to turn them face up. If the cards have the \r\n"+
						"same rank, then you have discovered a trio.  The trio will remain\r\n"+
						"turned up.  If the cards are different, they will flip back\r\n"+
						"over automatically after a short delay.  Continue flipping\r\n"+
						"cards until you have discovered all of the pairs.  The game\r\n"+
						"is won when all cards are face up.\r\n"+
						"Each time you flip three cards up, the turn counter will\r\n"+
						"increase.  Try to win the game in the fewest number of turns!"+
						"\r\n"+
						"FLUSH Level\r\n"+
						"The game consists of a grid of cards.  At the start of the game,\r\n"+
						"every card is face down.  The object is to find five cards with\r\n"+
						"the same suit and turn them face up.\r\n"+
						"\r\n"+
						"Click on five cards to turn them face up. If the cards have the \r\n"+
						"same suit, then you have discovered a flush.  The flush will remain\r\n"+
						"turned up.  If the cards are different, they will flip back\r\n"+
						"over automatically after a short delay.  The game\r\n"+
						"is won when there are no flush remaining on the grid.\r\n"+
						"\r\n"+
						"STRAIGHT Level\r\n"+
						"The game consists of a grid of cards.  At the start of the game,\r\n"+
						"every card is face down.  The object is to find five cards in sequence\r\n"+
						"with at least two different suits and trun them face up.\r\n"+
						"\r\n"+
						"Click on five cards to turn them face up. If the cards are in sequence and \r\n"+
						"have two distinct suits or more, then you have discovered a straight.  \r\n"+
						"The straight will remain turned up.  If the cards are different, \r\n"+
						"they will flip back over automatically after a short delay.  The game\r\n"+
						"is won when there are no straights remaining on the grid.\r\n"+
						"\r\n"+
						"Each time you flip two cards up, the turn counter will\r\n"+
						"increase.  Try to win the game in the fewest number of turns!\r\n"+
						"\r\n"+
						"COMBO Level\r\n"+
						"The game consists of a grid of cards.  At the start of the game,\r\n"+
						"every card is face down.  There are Seven possible hands here\r\n"+
						"(Straight, Flush, Hand Pair, Two Pair, Hand Trio , High Card & Full House).\r\n"+
						"Uncover one of these hands and decide wether you want to stay with it or not.\r\n"+
						"\r\n"+
						"Click on Seven cards to turn them face up.  If the cards are in sequence and\r\n"+
						"have two distinct suits or more, then you have discovered a Straight.\r\n" + 
						"If the cards have the same suit,  then you have discovered a Flush.\r\n"+
						"If two cards are the same value,  then you have discovered a Pair.\r\n"+
						"If there are two set of pairs in the same hand,\r\n"+
						"then you have discovered a Two Pair.  If three cards are the same value, \r\n"+
						"then you have discovered a Trio.  And if there is a Pair and a Trio in the \r\n"+ 
						"same hand, you have discovered a Full House.  The hand will remain turned up.\r\n"+
						"If the cards are different or you choose to pass, they will flip back \r\n"+
						"over automatically after a short delay.  The game is won when there are \r\n"+
						"no Possible Hands remaining on the grid.\r\n"+
						"\r\n"+
						"Try to Find the best Hands to get the Highest Score!!";

		text.setText(HOWTOPLAYTEXT);
		text.setEditable(false);
		JScrollPane textScroll = new JScrollPane(text);
		JOptionPane.showMessageDialog(this, textScroll
				, "How To Play", JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Shows an dialog box with information about the program
	 */
	private void showAbout()
	{
		dprintln("MemoryGame.showAbout()");
		final String ABOUTTEXT = "Game Customized at UPRM by Fernando Davis and Ray J. Ayende. Originally written by Mike Leonhard";

		JOptionPane.showMessageDialog(this, ABOUTTEXT
				, "About Memory Game", JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Displays a "cheat sheet" that basically turns the grid into string and displays the position of every card in order
	 */
	private void showCheats() 
	{	
		@SuppressWarnings("rawtypes")
		ArrayList grid = new ArrayList();
		grid = difficulty.getGrid();
		String ranks[] = new String[grid.size()];
		String suits[] = new String[grid.size()];
		
		for(int i = 0; i < grid.size(); i++)
		{
			ranks[i] = ((Card) grid.get(i)).getRank(); //Gets the rank of every card on the grid and stores it in a variable
			suits[i] = ((Card) grid.get(i)).getSuit(); //Gets the suit of every card on the grid and stores it in a variable
		}
		
		String cardNames[] = new String[grid.size()]; 
		
		for(int i = 0; i < grid.size(); i++) 
		{
			cardNames[i] = ranks[i] + suits[i]; //Makes an array of ranks and suits concatenated
		}
		String cheats = Arrays.toString(cardNames); //returns array in a string
		StringBuffer buffer = new StringBuffer();

		for(int i = 0; i < cheats.length(); i++) {
		    // Append a \n depending on the size of the grid
		    if(grid.size() == 16) {
		    	if((i > 0) && (i % grid.size()) == 0) {
		    		buffer.append("\n");
		    	}
		    }
		    else if(grid.size() == 50){
		    	if((i > 0) && (i % 40) == 0) {
		    		buffer.append("\n");
		    	}
		    }
		    // Just adds the next character to the StringBuffer.
		    buffer.append(cheats.charAt(i));
		}
		String newString = buffer.toString();
		JOptionPane.showMessageDialog(this, newString
				, "Cheats", JOptionPane.PLAIN_MESSAGE); //Displays strings in a window
	}
	
	/**
	 * Prints debugging messages to the console
	 *
	 * @param message the string to print to the console
	 */
	static public void dprintln( String message )
	{
		if (DEBUG) System.out.println( message );
	}
}

import javax.swing.JFrame;

public class RankTrioScore extends RankTrioLevel{
	
	private TurnScore Score;
	
	protected RankTrioScore(TurnsTakenCounterLabel validTurnTime, TurnScore Score, JFrame mainFrame) {
		super(validTurnTime, mainFrame);
		super.getTurnsTakenCounter().setDifficultyModeLabel("Trio Level");
		super.setCardsToTurnUp(3);
		super.setCardsPerRow(10);
		super.setRowsPerGrid(5);;
		this.Score = Score; Score.reset(); 
		this.getScore().setDifficultyModeLabel("Trio Level");
		
	}
	
	GameSounds sounds = new GameSounds();
	int counter = 0;
	
	@Override
	protected boolean turnUp(Card card) {
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{
			sounds.pikachu();
			this.getTurnedCardsBuffer().add(card);
			if(this.getTurnedCardsBuffer().size() == getCardsToTurnUp())
			{
				this.getTurnsTakenCounter().increment();
				Card otherCard1 = (Card) this.getTurnedCardsBuffer().get(0);
				Card otherCard2 = (Card) this.getTurnedCardsBuffer().get(1);
				if((card.getRank().equals(otherCard1.getRank())) && (card.getRank().equals(otherCard2.getRank()))) 
				{
					counter++;
					// when the 3 cards match the score will increment by 100 points.
					sounds.worked();
					this.getTurnedCardsBuffer().clear();
					this.getScore().increment(100);
					// will search for the rank and increase the score again based on the trio match rank.
					if((card.getRank().equals("a"))) {
						this.getScore().increment(20*3);
						}
					else if((card.getRank().equals("k"))) {
						this.getScore().increment(13*3);
						}
					else if((card.getRank().equals("q"))) {
						this.getScore().increment(12*3);
						}
					else if((card.getRank().equals("j"))) {
						this.getScore().increment(11*3);
						}
				}
				else
				{
					sounds.accomplished();//Plays sound
					// The cards don't match so the score will decrease by 5 points.
					this.getTurnDownTimer().start();
					this.getScore().increment(-5);
				}
				
			}
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean  isGameOver(){
		if(counter == 12) //If there's 12 hands turned face up, end game. 14 cards remain without match in grid.
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the score of the individual difficulty
	 */
	private TurnScore getScore() {
		// TODO Auto-generated method stub
		return Score;
	}

}

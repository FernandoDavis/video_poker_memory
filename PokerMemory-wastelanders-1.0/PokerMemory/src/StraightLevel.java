 import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class StraightLevel extends FlushLevel{

	protected StraightLevel(TurnsTakenCounterLabel validTurnTime, JFrame mainFrame) {
		super(validTurnTime, mainFrame);
		this.getTurnsTakenCounter().setDifficultyModeLabel("Straight Level");
		this.setCardsToTurnUp(5);
	}
	
	int variable = 0;
	
	@Override
	protected void makeDeck() {
		// In Straight level the grid consists of distinct cards, no repetitions

		//back card
		ImageIcon backIcon = this.getCardIcons()[this.getTotalCardsPerDeck()];

		int cardsToAdd[] = new int[getRowsPerGrid() * getCardsPerRow()];
	
		Random rand = new Random();
		int startingIndex = rand.nextInt(51);
		int i_1 = startingIndex;
		for(int i = 0 ; i < 50; i++)
		{
			if(i_1 >= 52)//If cards on the cardnames array pass 51 (index 52 is the back card) then reset at 0, otherwise increment by 1.
			{
				i_1 = 0;
				cardsToAdd[i] = i_1;
				i_1++;
			}
			else 
			{
				cardsToAdd[i] = i_1;
				i_1++;
			}
		}

		// randomize the order of the deck
		this.randomizeIntArray(cardsToAdd);

		// make each card object
		for(int i = 0; i < cardsToAdd.length; i++)
		{
			// number of the card, randomized
			int num = cardsToAdd[i];
			// make the card object and add it to the panel
			String rank = cardNames[num].substring(0, 1);
			String suit = cardNames[num].substring(1, 2);
			this.getGrid().add( new Card(this, this.getCardIcons()[num], backIcon, num, rank, suit));
		}
	}
	
	@Override
	protected boolean turnUp(Card card) {
		// the card may be turned
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{
			// add the card to the list
			this.getTurnedCardsBuffer().add(card);
			if(this.getTurnedCardsBuffer().size() == this.getCardsToTurnUp())
			{
				// We are uncovering the last card in this turn
				// Record the player's turn
				this.getTurnsTakenCounter().increment();
				// get the other card (which was already turned up)
				Card otherCard1 = (Card) this.getTurnedCardsBuffer().get(0);
				Card otherCard2 = (Card) this.getTurnedCardsBuffer().get(1);
				Card otherCard3 = (Card) this.getTurnedCardsBuffer().get(2);
				Card otherCard4 = (Card) this.getTurnedCardsBuffer().get(3);
				Card otherCard5 = (Card) this.getTurnedCardsBuffer().get(4);
				
				/*
				 * Assign the 5 face up cards to an array, so that you can check their index with respect to 
				 * their individual ranks, store the index in another array and sort to check straight.
				 */
				
				String[] straightCards = {otherCard5.getRank(), otherCard1.getRank(), otherCard2.getRank(), otherCard3.getRank(), otherCard4.getRank()};	
				int [] straightCardsIndex = new int[5];
				for(int i = 0; i < 13; i++) {	
					for(int j = 0; j < 5; j++) {
						if(ranks[i].equals(straightCards[j])) {
							straightCardsIndex[j] = i;
						}
					}
				}
				Arrays.sort(straightCardsIndex);				
				
				if((straightCardsIndex[0] + 1 == straightCardsIndex[1]) && (straightCardsIndex[1] + 1 == straightCardsIndex[2]) && (straightCardsIndex[2] + 1 == straightCardsIndex[3]) && straightCardsIndex[4] == 12){
					this.getTurnedCardsBuffer().clear();
				}
				else if((straightCardsIndex[0] + 1 == straightCardsIndex[1]) && (straightCardsIndex[1] + 1 == straightCardsIndex[2]) && (straightCardsIndex[2] + 1 == straightCardsIndex[3]) && (straightCardsIndex[3] + 1 == straightCardsIndex[4])){
					// 5 cards match, so remove them from the list (they will remain face up)
					this.getTurnedCardsBuffer().clear();
					variable = 0;
				}
				else
				{
					// The cards do not match, so start the timer to turn them down
					this.getTurnDownTimer().start();
					variable = 1;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	protected boolean  isGameOver()
	{

		ArrayList<Card> faceDownCards = new ArrayList<Card>();

		for(int i = 0; i < this.getGrid().size(); i++)
		{
			if(!this.getGrid().get(i).isFaceUp())
				faceDownCards.add(getGrid().get(i)); //Gets all face down cards and stores it in a ArrayList 
		}

		if(faceDownCards.size() == 15 && variable == 1){
			/*
			 * Once 15 cards are face down, if the player chooses a straight it adds the points and finishes, 
			 * otherwise just finishes without adding any points.
			 */
			return true;
		}
		
		if(faceDownCards.size() == 10 && variable == 0){
			/*
			 * Once 15 cards are face down, if the player chooses a straight it adds the points and finishes, 
			 * otherwise just finishes without adding any points.
			 */
			return true;
		}
		else
			return false;
		}
}
import java.util.Arrays;
import javax.swing.JFrame;

public class StraightScore extends StraightLevel {
	private TurnScore Score;
	protected StraightScore(TurnsTakenCounterLabel validTurnTime, TurnScore Score, JFrame mainFrame) {
		super(validTurnTime, mainFrame);
		this.getTurnsTakenCounter().setDifficultyModeLabel("Straight Level");
		this.setCardsToTurnUp(5);
		this.Score = Score; Score.reset(); 
		this.getScore().setDifficultyModeLabel("Straight Level");
	}
	private TurnScore getScore() {
		// TODO Auto-generated method stub
		return Score;
	}

	
	@Override
	protected boolean turnUp(Card card) {
		// the card may be turned
		if(this.getTurnedCardsBuffer().size() < getCardsToTurnUp()) 
		{
			// add the card to the list
			sounds.pikachu();
			this.getTurnedCardsBuffer().add(card);
			if(this.getTurnedCardsBuffer().size() == this.getCardsToTurnUp())
			{
				// We are uncovering the last card in this turn
				// Record the player's turn
				this.getTurnsTakenCounter().increment();
				// get the other card (which was already turned up)
				Card otherCard1 = (Card) this.getTurnedCardsBuffer().get(0);
				Card otherCard2 = (Card) this.getTurnedCardsBuffer().get(1);
				Card otherCard3 = (Card) this.getTurnedCardsBuffer().get(2);
				Card otherCard4 = (Card) this.getTurnedCardsBuffer().get(3);
				Card otherCard5 = (Card) this.getTurnedCardsBuffer().get(4);
				
				String[] straightCards = {otherCard5.getRank(), otherCard1.getRank(), otherCard2.getRank(), otherCard3.getRank(), otherCard4.getRank()};	
				int [] straightCardsIndex = new int[5];
				for(int i = 0; i < 13; i++) {	
					for(int j = 0; j < 5; j++) {
						if(ranks[i].equals(straightCards[j])) {
							straightCardsIndex[j] = i;
						}
					}
				}
				Arrays.sort(straightCardsIndex);				
				//The score in both cases increases by 100 plus the product of the rank by 100.
				if((straightCardsIndex[0] + 1 == straightCardsIndex[1]) && (straightCardsIndex[1] + 1 == straightCardsIndex[2]) && (straightCardsIndex[2] + 1 == straightCardsIndex[3]) && straightCardsIndex[4] == 12){
					sounds.worked();
					this.getTurnedCardsBuffer().clear();
					this.getScore().increment(1000);
					//loop to search for the highest card in the hand.
					int max1 = straightCardsIndex[4];
					for (int i = 0; i < straightCardsIndex.length; i++)
					{
							if (straightCardsIndex[i] > max1) 
							{
								max1 = straightCardsIndex[i];
							}
					}	
					if(max1 == (12)) {
						this.getScore().increment(100*20);
						}
					else 
						{
						this.getScore().increment((max1 + 2)*100);
						}	
				}
				else if((straightCardsIndex[0] + 1 == straightCardsIndex[1]) && (straightCardsIndex[1] + 1 == straightCardsIndex[2]) && (straightCardsIndex[2] + 1 == straightCardsIndex[3]) && (straightCardsIndex[3] + 1 == straightCardsIndex[4])){
					// 5 cards match, so remove them from the list (they will remain face up)
					sounds.worked();
					this.getTurnedCardsBuffer().clear();
					this.getScore().increment(1000);
					
					int max = straightCardsIndex[4];
					for (int i = 0; i < straightCardsIndex.length; i++)
					{
							if (straightCardsIndex[i] > max) 
							{
								max = straightCardsIndex[i];
							}
					}	
					if(max == (12)) {
						this.getScore().increment(100*20);
						}
					else 
						{
						this.getScore().increment((max + 2)*100);
						}
					
				}
				else
				{
					// The cards do not match, so the score decreases by 5 points.
					sounds.accomplished();
					this.getTurnDownTimer().start();
					this.getScore().increment(-5);
				}
			}
			return true;
		}
		return false;
	}
}
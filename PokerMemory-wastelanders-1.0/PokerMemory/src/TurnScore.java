import javax.swing.JLabel;

public class TurnScore extends JLabel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private int Score = 0;

		public TurnScore()
		{
			super();
			reset();
		}
		
		public void setDifficultyModeLabel(String difficultyMode){
		}
		
		public int getScore(){
			return this.Score;
			}
		
		/**
		 * Update the text label with the current score
		*/
		private void update()
		{
			this.setText("" + this.getScore());
		}
		
		/**
		 * Default constructor, starts counter at 0
		*/
		
		/**
		 * Increments the counter and updates the text label
		 * @param Score 
		*/
		public void increment(int Score)
		{
			this.Score = this.Score + Score ;
			update();
		}
		
		/**
		 * Resets the score
		*/
		public void reset()
		{
			this.Score= 0;
			update();
		}

	}

